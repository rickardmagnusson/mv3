﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mv3.Extensions
{
    /// <summary>
    /// Mv3 string extensions
    /// </summary>
    internal static class StringExtension
    {
        /// <summary>
        /// Simply makes a html space.
        /// </summary>
        /// <returns>Html space</returns>
        public static string Space()
        {
            return "&nbsp;";
        }


        /// <summary>
        /// Simply makes a html space.
        /// </summary>
        /// <returns>Html space</returns>
        public static string Space(this string s)
        {
            return "&nbsp;";
        }

        
        /// <summary>
        /// Controll if string is empty. 
        /// Return <see cref="Space"/> if empty.
        /// </summary>
        /// <param name="s">The string to check.</param>
        /// <returns>Space or value</returns>
        public static string ValueOrSpace(this string s)
        {
            return s.Length > 0 ? s : Space();
        }
    }
}