﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mv3.Conf;
using Mv3.Models;
using Mv3.ViewModels;

namespace Mv3.Extensions
{
    /// <summary>
    /// Mv3 Page extensions
    /// </summary>
    public static class PageExtensions
    {
        /// <summary>
        /// The current url
        /// </summary>
        /// <param name="page">Page p</param>
        /// <returns>The current <see cref="Mv3.Models.Page"/></returns>
        public static string CurrentUrl(this Page page)
        {
            return HttpContext.Current.Request.RawUrl.ToString().ToLower();
        }

        /// <summary>
        /// The current <see cref="Mv3.Models.Page"/>
        /// </summary>
        public static Page CurrentPage
        {
            get
            {
                return Host.Open().QueryOver<Page>().Where(p => p.Url == new Page().CurrentUrl()).List().First();
            }
        }
    }
}