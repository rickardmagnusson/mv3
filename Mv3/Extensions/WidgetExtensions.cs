﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mv3.Extensions
{
    public static class WidgetExtensions
    {
        /// <summary>
        /// Func : Return a boolean if the current Widget is Active and can be added to this Page and Zone.
        /// </summary>
        private static Func<dynamic, Region, bool> IsValid = (widget, region) =>
        {
            return widget.Region == region &&
                   widget.Active &&
                   widget.PageId == PageExtensions.CurrentPage.Id;
        };


        /// <summary>
        /// Enumerate each Widget to see if it can be added to current Page and current Zone.
        /// </summary>
        /// <typeparam name="T">IEnumerable list</typeparam>
        /// <param name="widgets">List of widgets</param>
        /// <param name="add">Action add</param>
        /// <param name="region">Region region</param>
        public static void AddEachValidWidget<T>(this IEnumerable<T> widgets, Action<T> add, Region region)
        {
            foreach (dynamic widget in widgets)
            {
                if (IsValid.Invoke(widget, region))
                    add(widget);
            }
        }
    }
}