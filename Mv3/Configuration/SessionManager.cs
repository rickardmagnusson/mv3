﻿using Mv3.Conf;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mv3.Configuration
{
    public sealed class SessionManager
    {
        ISession session;
        SessionManager()
        {
            ISessionFactory factory = Host.Factory();
            session = factory.OpenSession();
        }

        internal ISession GetSession()
        {
            return session;
        }

        public static SessionManager Instance
        {
            get
            {
                return Nested.instance;
            }
        }

        class Nested
        {
            // Explicit static constructor to tell C# compiler
            // not to mark type as beforefieldinit
            static Nested()
            {
            }

            internal static readonly SessionManager instance = new SessionManager();
        }
    }
}