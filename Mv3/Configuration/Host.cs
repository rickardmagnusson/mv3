﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using NHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using System.IO;
using NHibernate.Tool.hbm2ddl;
using FluentNHibernate.Automapping;
using Mv3.Data;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Mapping.ByCode;
using System.Reflection;
using NHibernate.Cfg;
using System.Data;
using Mv3.Models;
using NHibernate.Linq;
using Mv3.Mvc.Html;

namespace Mv3.Conf
{
    public static class Host
    {
        private static NHibernate.Cfg.Configuration _Configuration { get; set; }   
        public static ISessionFactory SessionFactory { get; set; }    

        public static void Init()
        {     
            #region NHibernate configuration      

            _Configuration = ConfigureNHibernate();      
            SessionFactory = _Configuration.BuildSessionFactory();  
    
            #endregion   
        }


        public static ISession Open()
        {
            if (SessionFactory == null)
                Host.Init();

            return SessionFactory.OpenSession();
        }

        public static ISessionFactory Factory()
        {
            if (SessionFactory == null)
                Host.Init();

            return SessionFactory;
        }
        

        public static void Close()
        {
            if (SessionFactory != null)
                SessionFactory.Close();
        }

        public static List<Page> QueryTest()
        {
            var session = Open();
            session.BeginTransaction();

            var query = from page in session.Linq<Page>() select page;
            Close();
            return query.ToList();
        }

        public static IList<T> FindResults<T>() where T : class
        {
            using (var session = Open()) { 
                var query = from a in session.Linq<T>() select a;
                return query.ToList();
            }
        }

        private static NHibernate.Cfg.Configuration ConfigureNHibernate()  
        {
            var configure = new NHibernate.Cfg.Configuration();     
            configure.SessionFactoryName("BuildContainer");     
            configure.DataBaseIntegration(db =>     
            {
                db.Dialect<NHibernate.Dialect.MsSql2008Dialect>();
                db.Driver<NHibernate.Driver.Sql2008ClientDriver>();       
                db.KeywordsAutoImport = Hbm2DDLKeyWords.AutoQuote;      
                db.IsolationLevel = IsolationLevel.ReadCommitted;
                db.ConnectionString = @"Data Source=(LocalDb)\v11.0;AttachDbFilename=|DataDirectory|\Mv3.mdf;Initial Catalog=Mv3;Integrated Security=True";       
                db.Timeout = 10;        // enabled for testing       
                db.LogFormattedSql = true;
                db.LogSqlInConsole = true;       
                db.AutoCommentSql = true;   
            });      
            var mapping = GetMappings();     
            configure.AddDeserializedMapping(mapping, "Mv3SchemaTest");    
            SchemaMetadataUpdater.QuoteTableAndColumns(configure);
            new SchemaUpdate(configure).Execute(false, true);

            return configure;   
        }    
        
        private static HbmMapping GetMappings()   
        {     
            var mapper = new ModelMapper();
            mapper.AddMappings(Assembly.GetAssembly(typeof(M3Entity<dynamic>)).GetExportedTypes());
            var mapping = mapper.CompileMappingForAllExplicitlyAddedEntities(); 
            return mapping;  
        }


    }
}