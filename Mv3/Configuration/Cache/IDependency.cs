﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mv3.Configuration.Cache
{
    public interface IDependency
    {
        Int32 Id { get; set; }
    }
}
