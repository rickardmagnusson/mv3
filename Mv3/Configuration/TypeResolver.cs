﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mv3.Configuration
{
    public class TypeResolver
    {
        /// <summary>
        /// List of Widget types
        /// </summary>
        /// <returns>List of Widget types</returns>
        public List<Type> GetAllWidgets()
        {
            var list = new List<Type>();
            var types = GetType().Assembly.GetTypes();
            list.AddRange(types.Where(t => t.IsClass && t.Name.Equals("Page") == false && (t.GetInterface("IDependency") != null) && (!t.IsAbstract)));
            return list;
        }
    }
}