﻿using NHibernate;
using NHibernate.Cfg;
using System.Data;



namespace Mv3.Configuration
{
    public class NHibernateHelper
    {
        private static volatile ISessionFactory sessionFactory;
        private static volatile NHibernate.Cfg.Configuration configuration;
        private static readonly object syncRoot = new object();

        public static ISessionFactory SessionFactory
        {
            get
            {
                if (sessionFactory == null)
                {
                    lock (syncRoot)
                    {
                        if (sessionFactory == null)
                        {
                            return sessionFactory = Configuration.BuildSessionFactory();
                        }
                    }
                }
                return sessionFactory;
            }
        }

        public static NHibernate.Cfg.Configuration Configuration
        {
            get
            {
                if (configuration == null)
                {
                    lock (syncRoot)
                    {
                        if (configuration == null)
                        {
                            var newConfig = new NHibernate.Cfg.Configuration();
                            newConfig.SessionFactoryName("BuildContainer");
                            newConfig.DataBaseIntegration(db =>
                            {
                                db.Dialect<NHibernate.Dialect.MsSql2008Dialect>();
                                db.Driver<NHibernate.Driver.Sql2008ClientDriver>();
                                db.KeywordsAutoImport = Hbm2DDLKeyWords.AutoQuote;
                                db.IsolationLevel = IsolationLevel.ReadCommitted;
                                db.ConnectionString = @"Data Source=(LocalDb)\v11.0;AttachDbFilename=|DataDirectory|\Mv3.mdf;Initial Catalog=Mv3;Integrated Security=True";
                                db.Timeout = 10;        // enabled for testing       
                                db.LogFormattedSql = true;
                                db.LogSqlInConsole = true;
                                db.AutoCommentSql = true;
                            });
                            return configuration = newConfig;
                        }
                    }
                }
                return configuration;
            }
        }

        public static ISession OpenSession()
        {
            ISession session = SessionFactory.OpenSession();
            session.DefaultReadOnly = true;
            return session;
        }
    }


}