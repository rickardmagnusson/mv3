﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mv3.Mvc.Html
{
    public static class ViewHelper
    {
        /// <summary>
        /// Renders a View
        /// </summary>
        /// <typeparam name="T">Model (T)</typeparam>
        /// <param name="html"></param>
        /// <param name="viewPath"></param>
        /// <param name="model">Model to render</param>
        /// <returns>Rendered Model</returns>
        public static string Render<T>(this HtmlHelper html, string viewPath, T model)
        {
            try
            {
                using (var writer = new StringWriter())
                {
                    var view = new WebFormView(html.ViewContext.Controller.ControllerContext, viewPath);
                    var vdd = new ViewDataDictionary<T>(model);
                    var context = new ViewContext(html.ViewContext.Controller.ControllerContext, view, vdd, new TempDataDictionary(), writer);
                    var v = new RazorView(html.ViewContext.Controller.ControllerContext, viewPath, null, false, null);
                    v.Render(context, writer);

                    return writer.ToString();
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}