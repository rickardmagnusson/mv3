﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Text;
using System.Reflection;
using System.Collections;
using NHibernate;
using NHibernate.Linq;
using Mv3.Conf;
using Mv3.Models;
using Mv3.ViewModels;
using Mv3.Extensions;
using Mv3.Configuration;

namespace Mv3.Mvc.Html
{
    public static partial class HtmlHelperExtensions
    {
        /// <summary>
        /// Render all Widgets in a Zone.
        /// </summary>
        /// <param name="html">HtmlHelper</param>
        /// <param name="region">Region region</param>
        /// <returns>Rendered HtmlString for a Zone.</returns>
        public static IHtmlString Zone(this HtmlHelper html, Region region)
        { 
            var sb = new StringBuilder();
            var session = SessionManager.Instance.GetSession();
            var widgetlist = new TypeResolver().GetAllWidgets(); //List of WidgetTypes in Assembly

             (from t in widgetlist
                        select session.CreateCriteria(t).List().As<System.Collections.ArrayList>()
                            into widgets
                            where widgets.Count > 0
                            from item in widgets.ToArray()
                            select item
                                into a
                                select a)
                                .AddEachValidWidget<dynamic>(widget => sb.Append(ViewHelper.Render<Widget>(html, widget.Template, widget)), region);

             return new HtmlString(sb.ToString().ValueOrSpace());
        }


        /// <summary>
        /// List of menuitems
        /// </summary>
        /// <param name="html"></param>
        /// <returns>A complete ul li menu</returns>
        public static MvcHtmlString Menu(this HtmlHelper html)
        {
            var session = SessionManager.Instance.GetSession();
            var list = session.QueryOver<Page>().List<Page>();
            var sb = new StringBuilder();

            TagBuilder ul = new TagBuilder("ul");
            ul.AddCssClass("nav nav-pills pull-right");

            foreach (var item in list) {
               sb.Append(html.Link(item.Title, item.Action, item.Controller, false, string.Empty, Direction.Horizontal, item.Id)); 
            }
            ul.InnerHtml = sb.ToString();

            return new MvcHtmlString(ul.ToString());
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="html"></param>
        /// <param name="labelText"></param>
        /// <param name="action"></param>
        /// <param name="controller"></param>
        /// <param name="isAdmin"></param>
        /// <param name="cssClass"></param>
        /// <param name="direction"></param>
        /// <returns></returns>
        public static MvcHtmlString Link(this HtmlHelper html, string labelText, string action, string controller, bool isAdmin, string cssClass, Direction direction, Int32 id)
        {
            string value = string.Empty;

            TagBuilder li = new TagBuilder("li");
            TagBuilder anchor = new TagBuilder("a");
            UrlHelper urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);

            //Controll if its an expandable menu.
            if (string.IsNullOrEmpty(action) || string.IsNullOrEmpty(controller)) {
                anchor.MergeAttribute("href", "#");
            } else {
                var target = (id == 0) ? "" : "" + id;
                if(isAdmin)
                anchor.MergeAttribute("href", urlHelper.Action(action, controller, new {
                    id = target
                }));
                else
                anchor.MergeAttribute("href", urlHelper.Action(action, controller, new
                {
                    
                }));
            }
            anchor.SetInnerText(labelText);

            //Is it the current route?
            if (action.Equals(html.ViewContext.RouteData.Values["action"].ToString())){
                li.MergeAttribute("class", "active");
            }

            //Add if any extra css classes
            if (!string.IsNullOrEmpty(cssClass)){
                li.MergeAttribute("class", cssClass);
            }

            li.InnerHtml = anchor.ToString(TagRenderMode.Normal);

            return MvcHtmlString.Create(li.ToString());           
        }
    }
}

