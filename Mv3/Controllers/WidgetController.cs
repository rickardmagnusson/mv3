﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Mv3.ViewModels;

namespace Mv3.Controllers
{
    public abstract class WidgetController<T> : Controller where T : class
    {
        dynamic Model;

        public WidgetController() : base()
        {
            Model = new WidgetViewModel<T>();
        }

        public ActionResult Index(Int32 id = 1)
        {
            return View(string.Format("~/Views/Widgets/{0}/Index.cshtml", typeof(T).Name), Model.Get(id));
        }

        
        public ActionResult Edit(Int32 id)
        {
            return View(string.Format("~/Views/Widgets/{0}/Edit.cshtml", typeof(T).Name), Model.Get(id));
        }

        
        public ActionResult Update(Int32 id)
        {
            return View(string.Format("~/Views/Widgets/{0}/Update", typeof(T).Name));
        }

        
        public ActionResult Delete(Int32 id)
        {
            return View();
        }
	}
}