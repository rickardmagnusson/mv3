﻿using Mv3.Models;
using Mv3.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mv3.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View(Model);
        }

        public ActionResult About()
        {
            return View(Model);
        }
    }
}