﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mv3.Models;
using Mv3.ViewModels;

namespace Mv3.Controllers
{
    public abstract class BaseController : Controller
    {
        public BaseController() : base()
        {
            Model = new PageViewModel();   
        }

        public PageViewModel Model
        {
            get;
            set;
        }
	}
}