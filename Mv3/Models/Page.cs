﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using Mv3.Data;
using Mv3.Configuration.Cache;
using Mv3.Conf;

namespace Mv3.Models
{
    public class Page : IDependency
    {
        public virtual Int32 Id { get; set; }
        public virtual Int32 ParentId { get; set; }
        public virtual string Title { get; set; }   
        public virtual string Url { get; set; }      //Used for redirect
        public virtual string MetaTitle { get; set; }
        public virtual string Controller { get; set; }
        public virtual string Action { get; set; }
        public virtual bool Active { get; set; }
    }

    public class PageMap : M3Entity<Page>
    {
        public PageMap() 
        {
            Id(p => p.Id, map => map.Generator(Generators.Assigned)); 
            Property(p => p.Title);
            Property(p => p.Url); 
            Property(p => p.ParentId);
            Property(p => p.Active);
            Property(p => p.MetaTitle);
            Property(p => p.Controller);
            Property(p => p.Action);
        } 
    } 
}