﻿using Mv3.Configuration.Cache;
using Mv3.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Mv3.Models
{
    public abstract class Widget : IDependency
    {
        protected Widget() { }

        public Int32 Id { get; set; }
        public Int32 PageId { get; set; }
        public Region Region { get; set; }
        public bool Active { get; set; }
        protected internal virtual string Template { get { return string.Format("~/Views/Widgets/{0}/Index.cshtml", this.GetType().Name); } }
    }
}