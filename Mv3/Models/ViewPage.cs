﻿using Mv3.Mvc.Html;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Mv3.Models
{
    /// <summary>
    /// Base class for all Views. Contain HtmlExtension helpers.
    /// </summary>
    /// <typeparam name="TModel">A dynamic model as strongly typed class</typeparam>
    public abstract class ViewPage<TModel> : WebViewPage<TModel>
    {
        private static ViewContext context;
        private static IViewDataContainer container;
        private readonly HtmlHelperFactory factory = new HtmlHelperFactory();
        public static new HtmlHelper<TModel> Html { get; private set; }

        private void CreateHelper(TModel model)
        {
            Html = factory.CreateHtmlHelper(model, new StringWriter(new StringBuilder()));
        }

        internal virtual TModel BaseModel
        {
            get { return base.Model; }
            set { CreateHelper(value); }
        }

        /// <summary>
        /// Initialize all helpers
        /// </summary>
        public override void InitHelpers()
        {
            base.InitHelpers();
            context = ViewContext;
            container = this;
            Html = new HtmlHelper<TModel>(context, container);
        }



        #region Expose methods for views

        public static dynamic Zone(Region region)
        {
            return Html.Zone(region);
        }

        public static dynamic Link(string label, string action, string controller, bool isAdmin, string cssClass, Direction direction, Int32 id = 0)
        {
            return Html.Link(label, action, controller, isAdmin, cssClass, direction, id);
        }

        public static dynamic Menu()
        {
            return Html.Menu();
        }

        #endregion


    }
}