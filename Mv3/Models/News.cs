﻿using Mv3.Data;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mv3.Models
{
    public class News : Widget
    {
        public virtual new Int32 Id { get; set; }
        public virtual string Title { get; set; }
        public virtual string Html { get; set; }
        public virtual new Int32 PageId { get; set; }
        public virtual new Region Region { get; set; }
        public virtual new bool Active { get; set; }

        public News() { }
    }

    public class NewsMap : M3Entity<News>
    {
        public NewsMap()
        {
            Lazy(true);
            Id(p => p.Id, map => map.Generator(Generators.Assigned));
            Property(p => p.Title);
            Property(p => p.Html);
            Property(p => p.PageId);
            Property(p => p.Region);
            Property(p => p.Active);
        }
    } 
}