﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using Mv3.Data;

namespace Mv3.Models
{
    public class Content : Widget
    {
        public virtual new Int32 Id { get; set; }
        public virtual string Title { get; set; }
        public virtual string Html { get; set; }
        public virtual new Int32 PageId { get; set; }
        public virtual new Region Region { get; set; }
        public virtual new bool Active { get; set; }
        public virtual bool CreateLink { get; set; }
        public virtual bool UseTitle { get; set; }

        public Content() : base() { }
    }

    public class ContentMap : M3Entity<Content>
    {
        public ContentMap() : base()
        {
            Lazy(true);
            Id(p => p.Id, map => map.Generator(Generators.Assigned));
            Property(p => p.Title);
            Property(p => p.Html);
            Property(p => p.PageId);
            Property(p => p.Region);
            Property(p => p.Active);
            Property(p => p.CreateLink);
            Property(p => p.UseTitle);
        }
    } 
}