﻿using Mv3.Conf;
using Mv3.Configuration.Cache;
using Mv3.Models;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mv3.ViewModels
{
    public class PageViewModel
    {
        private NHibernate.ISession _Connection { get; set; }
      

        /// <summary>
        /// Contains Page specific operations.
        /// </summary>
        public PageViewModel() {
            _Connection = Host.Open();
           
        }

        /// <summary>
        /// List of Pages
        /// </summary>
        public IList<Page> Pages
        {
            get { return FindResults<Page>(); }
        }

        /// <summary>
        /// Page from Id
        /// </summary>
        /// <param name="id">The id to search for.</param>
        /// <returns>Page from specified id</returns>
        public Page Page(Int32 id)
        {
            return FindResult<Page>(id);
        }

        /// <summary>
        /// The Current Page based from Url.
        /// </summary>
        public Page Current
        {
            get
            {
                var url = HttpContext.Current.Request.RawUrl.ToString().ToLower();
                return _Connection.QueryOver<Page>().Where(p => p.Url == url).List().First();
            }
        }

        public IList<Widget> Widgets
        {
            get
            {
                return _Connection.QueryOver<Widget>().Where(w => w.PageId == Current.Id).List();
            }
        }

        public IList<Widget> WidgetIn(Region region)
        {
            return (from a in _Connection.Linq<Widget>() select a).Where(w=> w.Region==region).ToList();
        }



        #region Dynamic operations

        /// <summary>
        /// Create's a list of the Type specified.
        /// </summary>
        /// <typeparam name="T">Type to get</typeparam>
        /// <returns>A IList of Type T</returns>
        private IList<T> FindResults<T>() where T : class
        {
            using (var con = _Connection)
            {
                return (from a in con.Linq<T>() select a).ToList();
            }
        }

        /// <summary>
        /// Generic select Type from a given Id.
        /// </summary>
        /// <typeparam name="T">Type T</typeparam>
        /// <param name="c">The Id to return item for.</param>
        /// <returns>Object</returns>
        private T FindResult<T>(Int32 c) where T : IDependency
        {
            using (var con = _Connection)
            {
                return (from a in con.Linq<T>()
                            where a.Id.Equals(c)
                            select a).ToList().First();
            }
        }


        #endregion
    }
}