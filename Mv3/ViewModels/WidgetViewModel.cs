﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mv3.Conf;
using Mv3.Models;
using NHibernate.Linq;

namespace Mv3.ViewModels
{
    public class WidgetViewModel<T> where T : class
    {
        public WidgetViewModel()
        {
  
        }

        /*
        public T Get(Int32 id) 
        {
            return Host.Open().Get<T>(id);
        }*/

        
        /// <summary>
        /// The widget from Id
        /// </summary>
        public Widget Get(Int32 id)
        {
            return Host.Open().Query<T>().Cast<Widget>().Where(w=> w.Id==id).First();
        }
    }
}