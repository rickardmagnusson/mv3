﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Predefined Regions. Add more if needed.
/// </summary>
public enum Region
{
    Header = 0, 
    Navigation = 1, 
    Featured = 2, 
    BeforeMain = 3, 
    AsideFirst = 4, 
    Messages = 5, 
    BeforeContent = 6, 
    Content = 7, 
    AfterContent = 8, 
    AsideSecond = 9, 
    AfterMain = 10, 
    TripelFirst = 11, 
    TripelSecond = 12, 
    TripelThird = 13, 
    FooterQuadFirst = 14, 
    FooterQuadSecond = 15, 
    FooterQuadThird = 16, 
    FooterQuadFourth = 17, 
    Footer = 18
}