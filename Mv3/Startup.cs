﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Mv3.Startup))]
namespace Mv3
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            Mv3.Conf.Host.Init();
        }
    }
}
