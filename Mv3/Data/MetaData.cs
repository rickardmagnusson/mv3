﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;
using NHibernate.Mapping.ByCode;
using Mv3.Configuration.Cache;

namespace Mv3.Data
{
    public class MetaData : IDependency
    {
        public virtual Int32 Id { get; set; }
        public virtual string MetaName { get; set; }
        public virtual string MetaContent { get; set; }
    }

    public class MetaDataMap : M3Entity<MetaData>
    {
        public MetaDataMap()
        {
            Id(p => p.Id, map => map.Generator(Generators.Assigned));
            Property(p => p.MetaName);
            Property(p => p.MetaName);
        }   
    }
}