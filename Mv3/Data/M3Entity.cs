﻿using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mv3.Data
{
    public abstract class M3Entity<T> : ClassMapping<T> where T : class
    {
        protected T data;

        protected M3Entity() : base()
        {
        }

        protected M3Entity(int id)
        {
            data = (T)Activator.CreateInstance(typeof(T));
        }
    }
}